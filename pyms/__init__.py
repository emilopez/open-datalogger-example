# -*- coding: utf-8 -*-
'''
    PyMs: Python Monitoring Station
    -------------------------------

    The public API and command-line interface to various monitoring stations.

    :copyright: Copyright 2015 Emiliano López et al, see AUTHORS.
    :license: GNU GPL v2.

'''
from .datalogger import OpenDatalogger