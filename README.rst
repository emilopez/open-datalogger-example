Software to dialog with open dataloggers.

Description
'''''''''''
We call *open dataloggers* those devices not tied to any particular driving software, and readily accessible with a
custom development.

You need a serial connection (USB or RS232) between your computer and the datalogger.

How to use it
'''''''''''''

- You need import the class OpenDatalogger

.. code:: python

    from pyms import OpenDatalogger

- Set the phisical USB/Serial connection, baudrate, and the datalogger model

.. code:: python

    PORT = '/dev/ttyUSB0'   # detected operating system port (over GNU/Linux)
    BAUDRATE = 115200       # communication baudrate
    MODEL = "DOTLOGGER"     # supported model for this example
    
    # Create the instance, start conn & get data
    Stevens = OpenDatalogger(PORT, BAUDRATE, MODEL)
    
- Start the connection and get realtime data

.. code:: python

    Stevens.start()
    realtime_data = Stevens.get_realtime_data()
    
You can find an example in example.py
