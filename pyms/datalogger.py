import serial, time
from datetime import *
import logging as log

class OpenDatalogger:
    '''Interface class to digaloge with open dataloggers'''
    log.basicConfig(filename='sys.log',level=log.DEBUG)
    def __init__(self,port,baudrate,model,parity=serial.PARITY_NONE,rtscts=0,xonxoff=0):
        self.commands = {"DOTLOGGER":{"cmdRT":"CD", "cmdData":"DB"}}
        self.set_model(model)
        try:
            self.serial = serial.Serial(port,baudrate,parity=parity,
                rtscts=rtscts,xonxoff=xonxoff)
        except serial.serialutil.SerialException:
            log.error("Serial port connection error")

    def supported_datalogger(self):
        '''return the list of supported dataloggers'''
        return [i for i in self.commands]
        
    def set_model(self, model):
        '''set the datalogger model and its commands'''
        if model in self.supported_datalogger():
            self.model = model
            self.cmdRT = self.commands[self.model]["cmdRT"]
            self.cmdData = self.commands[self.model]["cmdData"]
        else:
            sms = "Not supported datalogger"
            log.error(sms)
            print(sms)
            
    def start(self):
        '''Start serial connection'''
        try:
            self.serial.open()
        except self.serial.SerialException as e:
            log.error("Error opening port %s: %s\n" % (self.serial.portstr, e))
        log.info("Communication was started")
        
    def close(self):
        '''Close serial connection'''
        self.serial.close()
        log.info("Communication was closed")
        
    def set_parameters(self,port, baudrate):
        '''Set port and baudrate'''
        self.serial = serial.Serial(port, baudrate)
        log.info("Settings were updated")
        
    def get_realtime_data(self):
        '''get and return realtime data'''
        log.info("Inicio Obtencion tiempo real")
        while self.serial.inWaiting() == 0:
            self.serial.write('\r\n')
            time.sleep(1)
        self.serial.write(self.cmdRT+'\r\n')
        out = ''
        time.sleep(1)
        while self.serial.inWaiting() > 0:
            out += self.serial.read(1)
        if out != '':
            return out